<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./public/css/style.css" rel="stylesheet">
    
    <title><?=$titre?></title>
</head>
<body>
    
    <header class="header">
        <img src="./public/img/movie.svg" alt="logo" />
        
        <div class="navi">
                <a href="http://localhost:8888/KOUZEHA_Ammar/wikicinema/homePage.php">Home</a>
                <a href="http://localhost:8888/KOUZEHA_Ammar/wikicinema/index.php?action=listFilms">Flims</a>
                <a href="http://localhost:8888/KOUZEHA_Ammar/wikicinema/index.php?action=listActeurs">Acteurs</a>
                <a href="http://localhost:8888/KOUZEHA_Ammar/wikicinema/index.php?action=listRealisateurs">Réalisateurs</a>
                <a href="http://localhost:8888/KOUZEHA_Ammar/wikicinema/index.php?action=listGenres">Genres</a>
                <a href="http://localhost:8888/KOUZEHA_Ammar/wikicinema/index.php?action=listeRoles">Rôles</a>
                <div class="dropdown">
                    <button onclick="myFunction()" class="dropbtn">Ajouter</button>
                    <div id="myDropdown" class="dropdown-content">
                        <a href="http://localhost:8888/KOUZEHA_Ammar/wikicinema/index.php?action=addFilm">Film</a>
                        <a href="http://localhost:8888/KOUZEHA_Ammar/wikicinema/index.php?action=addActeur">Acteur</a>
                        <a href="http://localhost:8888/KOUZEHA_Ammar/wikicinema/modifierRealisateur.php">Réalisateur</a>
                        <a href="http://localhost:8888/KOUZEHA_Ammar/wikicinema/modifierGenre.php">Genre</a>
                        <a href="http://localhost:8888/KOUZEHA_Ammar/wikicinema/modifierRole.php">Rôle</a>
                    </div>
                </div>
        </div>
    </header>


    <h3><?=$titreSecondaire?></h3>
    

    
    <div id="wrapper">
        <?= $content ?>
    </div>


    <script>
        /* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}


    </script>

</body>
    

</html>