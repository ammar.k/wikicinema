<?php
ob_start();
$film = $requete->fetch();
$name = str_replace(' ', '_', $film['titre']);
?>
    
    <div class="displayFilm">
    <img src="./public/img/<?= $name?>.jpg" alt="affiche" width="300px" height="350px">
    
    <div class="informationFilm">
    <p> Titre :<?= $film['titre']?> </p>
    <p> Année de sortie :  <?= $film['annee_sortie'] ?></p>
    <p> Note :  <?= $film['note']."/5" ?></p>
    <p> Durée :  <?= $film['duree'] ." Minutes" ?></p>
    <p> Resumé :  <?= $film['synopsis'] ?></p>
    <p> Réalisateur :  <?= $film['nom']." ".$film['prenom'] ?></p>
    <p> Genre :  <?= $film['libelle'] ?></p>
            
    </div>
        
    </div>

<?php
$titre = "Info Film";
$titreSecondaire = "Info Film";
$content = ob_get_clean();
require "template.php";
?>