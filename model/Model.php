<?php
namespace Model;

//class abstract because we don't want to create an instance of this class
abstract class Connect{
    const HOST = "localhost";
    const DB = "cinema";
    const USER = "root";
    const PASS = "root";


    public static function seConnecter() {
        try {
            return new \PDO(
                "mysql:host=".self::HOST.";dbname=".self::DB.";charset=utf8", self::USER, self::PASS);
            
        } catch (\PDOException $ex) {
            return $ex->getMessage();
        }
    }
}

?>