<?php
ob_start();

?>

<div class="container-ajouter">
        <form action="index.php?action=addFilm" method="post">
            <div class="row">
                <div class="col-25">
                    <label for="titre">Titre</label>
                </div>
                <div class="col-75">
                    <input type="text" id="tname" name="titre" placeholder="Titre ..">
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="anne">Année De Sortie</label>
                </div>
                <div class="col-75">
                    <input type="text" id="anne" name="annee_sortie" placeholder="Année De Sortie ..">
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="text">Durée</label>
                </div>
                <div class="col-75">
                    <input type="number" id="duree" name="duree" min="10" placeholder="Durée En Minutes (10 minimum)">
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="text">Note</label>
                </div>
                <div class="col-75">
                    <input type="number" id="note" name="note" min="1" placeholder="Note sur 5">
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="Affiche">Affiche de Film</label>
                </div>
                <div class="col-75">
                    <input type="text" id="affiche" name="affiche_film" placeholder="affiche.jpg">
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <label for="réal">Réalisateur</label>
                </div>
                <div class="col-75">
                    <select id="réal" name="réalisateur">
                        <?php
                            foreach($requeteRealisateur as $realisateur) { ?>
                                <option value="<?= $realisateur["id_realisateur"] ?>"><?= $realisateur["realisateurFilm"] ?> </option>
                            <?php }
                        ?>

                    </select>
                </div>
                </div>
            <div class="row">
                <div class="col-25">
                    <label for="genre">Genre</label>
                </div>
                <div class="col-75">
                    <select id="genre" name="libelle">
                        <?php
                            foreach($requeteGenres as $genre) { ?>
                                <option value="<?= $genre["id_genre"] ?>"><?= $genre["libelle"] ?> </option>
                            <?php }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="synopsis">Synopsis</label>
                </div>
                <div class="col-75">
                    <textarea id="synopsis" name="synopsis" placeholder="Synopsis.." style="height:200px"></textarea>
                </div>
            <br>
            <div class="row">
                <input type="submit" name="submit" value="Ajouter">
            </div>
        </form>
        
    </div>
    <div class="success-message">   
        <?php
    if(isset($_SESSION['message'])){

        echo "<p>" . $_SESSION['message'] . "</p>";
        unset($_SESSION['message']);
    }
    ?>
    </div>
          
<?php
$titre = "Ajouter un film";
$titreSecondaire = "Ajouter un film";
$content = ob_get_clean();
require "view/template.php";
?>
