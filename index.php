<?php
    use Controller\CinemaController;
    //spl_autoload_register(function ($class_name) {
      //  include $class_name . '.php';
    //});
    

    require_once "controller/CinemaController.php";
    
    $ctrlCinema = new CinemaController();
    $id = (isset($_GET['id'])) ? $_GET['id'] : null;
    if(isset($_GET["action"])) {
        switch ($_GET["action"]){
            case 'listFilms': $ctrlCinema->listFilms(); break;
            case 'listActeurs': $ctrlCinema->listActeurs(); break;
            case 'listGenres': $ctrlCinema->listGenres(); break;
            case 'listRealisateurs': $ctrlCinema->listRealisateurs(); break;
            case 'listeRoles': $ctrlCinema->listeRoles(); break;
            case 'addFilm': $ctrlCinema->addFilm(); break;
            case 'infoFilm': $ctrlCinema->infoFilm($id); break;
            case 'infoActeur': $ctrlCinema->infoActeur($id); break;
            case 'infoRealisateur': $ctrlCinema->infoRealisateur($id); break;
            case 'infoGenre': $ctrlCinema->infoGenre($id); break;
            case 'infoRole': $ctrlCinema->infoRole($id); break;
            case 'addActeur': $ctrlCinema->addActeur(); break;
            case 'addFilm': $ctrlCinema->addFilm(); break;
        }
    }
?>

