<?php
ob_start();
?>
    <div class="container-ajouter">
        <form action="index.php?action=addActeur" method="post">
            <div class="row">
                <div class="col-25">
                    <label for="lname">Nom</label>
                </div>
                <div class="col-75">
                    <input type="text" id="lname" name="nom" placeholder="Nom..">
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="fname">Prenom</label>
                </div>
                <div class="col-75">
                    <input type="text" id="fname" name="prenom" placeholder="Prenom..">
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="date">Date de naissance</label>
                </div>
                <div class="col-75">
                    <input type="text" id="date" name="dateNaissance" placeholder="YYYY-MM-DD">
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="sex">Sexe</label>
                </div>
                <div class="col-75">
                    <select id="sex" name="sex">
                        <option value="M">M</option>
                        <option value="F">F</option>
                    </select>
                </div>
            </div>
            <br>
            <div class="row">
                <input type="submit" value="Ajouter">
            </div>
        </form>
        
    </div>
    <div class="success-message">   
        <?php
    if(isset($_SESSION['message'])){

        echo "<p>" . $_SESSION['message'] . "</p>";
        unset($_SESSION['message']);
    }
    ?>
    </div>
    
          
<?php
$titre = "Ajouter un acteur";
$titreSecondaire = "Ajouter un acteur";
$content = ob_get_clean();
require "view/template.php";
?>