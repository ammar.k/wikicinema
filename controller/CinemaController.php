<?php

namespace Controller;
use Model\Connect;


require_once "model/Model.php";


class CinemaController{
    /*
        Listes des films
     */
    public function listFilms(){
        $pdo = Connect::seConnecter();
        $requete = $pdo->query("SELECT titre, id_film FROM FILM");
        require_once "view/ListeFilms.php";
    }

    public function listActeurs(){
        $pdo = Connect::seConnecter();
        $requete = $pdo->query("SELECT ACTEUR.id_acteur AS id_acteur, 
        nom, prenom, YEAR(NOW()) - YEAR(dateNaissance) AS age,sex
        FROM PERSONNE
        INNER JOIN ACTEUR ON PERSONNE.id_personne=ACTEUR.id_personne " );
        require_once "view/listeActeurs.php";
    }

    public function listGenres(){
        $pdo = Connect::seConnecter();
        $requete = $pdo->query("SELECT id_genre, libelle FROM GENRE");
        require_once "view/listeGenres.php";
    }

    public function listRealisateurs(){
        $pdo = Connect::seConnecter();
        $requete = $pdo->query("SELECT REALISATEUR.id_realisateur, nom, prenom, YEAR(NOW()) - YEAR(dateNaissance) AS age,sex
        FROM PERSONNE
        INNER JOIN REALISATEUR ON PERSONNE.id_personne=REALISATEUR.id_personne ");
        require_once "view/listeRealisateur.php";
    }

    public function listeRoles(){
        $pdo = Connect::seConnecter();
        $requete = $pdo->query("SELECT id_role, nom_role FROM ROLES");
        require_once "view/listeRoles.php";
    }


    public function infoFilm($id){
        $pdo = Connect::seConnecter();
        $requete = $pdo->prepare("SELECT titre, annee_sortie, duree, synopsis,
        note, PERSONNE.nom, PERSONNE.prenom, GENRE.libelle
        FROM FILM
        INNER JOIN REALISATEUR ON FILM.id_realisateur=REALISATEUR.id_realisateur
        INNER JOIN PERSONNE ON REALISATEUR.id_personne = PERSONNE.id_personne
        INNER JOIN appartenir ON FILM.id_film=appartenir.id_film
        INNER JOIN GENRE ON appartenir.id_genre= GENRE.id_genre
        WHERE FILM.id_film = :id_film");
        $requete->execute([
            "id_film" => $id
        ]);
        require_once "view/infoFilm.php";
    }

    public function infoActeur($id){
        $pdo = Connect::seConnecter();
        $requete = $pdo->prepare("SELECT nom, prenom, dateNaissance,sex
        FROM PERSONNE
        INNER JOIN ACTEUR ON PERSONNE.id_personne = ACTEUR.id_personne
        WHERE ACTEUR.id_acteur = :id_acteur;");
        $requete2 = $pdo->prepare("SELECT FILM.id_film,titre
        FROM FILM
        INNER JOIN acter ON FILM.id_film=acter.id_film
        INNER JOIN ACTEUR ON acter.id_acteur=ACTEUR.id_acteur
        WHERE ACTEUR.id_acteur = :id_acteur;");
        $requete->execute([
            "id_acteur" => $id
        ]);
        $requete2->execute([
            "id_acteur" => $id
        ]);
        require_once "view/infoActeur.php";
    }

    public function infoRealisateur($id){
        $pdo = Connect::seConnecter();
        $requete = $pdo->prepare("SELECT nom, prenom, dateNaissance,sex
        FROM PERSONNE
        INNER JOIN REALISATEUR ON PERSONNE.id_personne = REALISATEUR.id_personne
        WHERE REALISATEUR.id_realisateur = :id_realisateur;");

        $requete2 = $pdo->prepare("SELECT FILM.id_film,titre
        FROM FILM
        INNER JOIN REALISATEUR ON FILM.id_realisateur = REALISATEUR.id_realisateur
        WHERE REALISATEUR.id_realisateur = :id_realisateur;");

        $requete->execute([
            "id_realisateur" => $id
        ]);
        $requete2->execute([
            "id_realisateur" => $id
        ]);
        require_once "view/infoRealisateur.php";
    }

    public function infoGenre($id){

        $pdo = Connect::seConnecter();
        $requete = $pdo->prepare("SELECT id_genre, libelle FROM GENRE WHERE id_genre = :id_genre");

        $requete2 = $pdo->prepare("SELECT FILM.id_film, GENRE.id_genre, titre
        FROM FILM
        INNER JOIN appartenir ON appartenir.id_film=FILM.id_film
        INNER JOIN GENRE ON GENRE.id_genre=appartenir.id_genre
        WHERE GENRE.id_genre = :id_genre");


        $requete->execute([
            "id_genre" => $id
        ]);

        $requete2->execute([
            "id_genre" =>$id
        ]);
        require_once "view/infoGenre.php";
    }

    public function infoRole($id){
        $pdo = Connect::seConnecter();
        $requete = $pdo->prepare("SELECT id_role, nom_role FROM ROLES WHERE id_role = :id_role");

        $requete2 = $pdo->prepare("SELECT FILM.id_film, ROLES.nom_role,ACTEUR.id_acteur, FILM.titre, prenom, nom
        FROM acter
        INNER JOIN ACTEUR ON ACTEUR.id_acteur = acter.id_acteur
        INNER JOIN FILM ON FILM.id_film = acter.id_film
        INNER JOIN PERSONNE ON ACTEUR.id_personne = PERSONNE.id_personne
        INNER JOIN ROLES ON ROLES.id_role = acter.id_role
        WHERE ROLES.id_role = :id_role;");

        $requete->execute([
            "id_role" => $id
        ]);

        $requete2->execute([
            "id_role" =>$id
        ]);
        require_once "view/infoRole.php";
    }

    public function addActeur(){
        $pdo = Connect::seConnecter();
        //filter the input
        $nom = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $dateNaissance = filter_input(INPUT_POST, 'dateNaissance', FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        $sex = $_POST['sex'];

        if($nom && $prenom && $dateNaissance){
            

            $requete = $pdo->prepare("INSERT INTO PERSONNE (nom,prenom,dateNaissance,sex)
            VALUES (:nom,:prenom,:dateNaissance,:sex) ");
            $requete->execute([
                'nom' => $nom,
                'prenom' => $prenom,
                'dateNaissance' => $dateNaissance,
                'sex' => $sex
            ]);


            
            $idPersonne = $pdo->lastInsertId();

            $requete2 = $pdo->prepare("INSERT INTO ACTEUR (id_personne) VALUES (:idPersonne)");

            $requete2->execute(['idPersonne' => $idPersonne]);
            

        }
        $_SESSION['message'] = "Acteur has been added successfully";
        require_once "ajouterActeur.php";

    }

    public function addFilm(){
        $pdo = Connect::seConnecter();
            //filter the input
            
            $requeteRealisateur = $pdo->prepare("SELECT CONCAT(prenom, ' ', nom) 
            AS realisateurFilm FROM realisateur r INNER JOIN personne p ON r.id_personne = p.id_personne");
            $requeteGenres = $pdo->prepare("SELECT * FROM genre");
    
            $requeteRealisateur->execute();
            $requeteGenres->execute();
    
            // requete genres
    
            if(isset($_POST["submit"])){
    
            $titre = filter_input(INPUT_POST, 'titre', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $annee_sortie = filter_input(INPUT_POST, 'annee_sortie', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $duree = filter_input(INPUT_POST, 'duree', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $synopsis = filter_input(INPUT_POST, 'synopsis', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $note = filter_input(INPUT_POST, 'note', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $affiche_film = filter_input(INPUT_POST, 'affiche_film', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $realisateur = filter_input(INPUT_POST, 'réalisateur', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $libelle = filter_input(INPUT_POST, 'libelle', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    
            if($titre && $annee_sortie && $duree && $synopsis && $note && $affiche_film && $realisateur && $libelle){
                $requete1 = $pdo->prepare("INSERT INTO FILM (titre,annee_sortie,duree,synopsis,note,affiche_film,id_realisateur)
                VALUES (:titre,:annee_sortie,:duree,:synopsis,:note,:affiche_film,:id_realisateur) ");
                $requete1->execute([
                    'titre' => $titre,
                    'annee_sortie' => $annee_sortie,
                    'duree' => $duree,
                    'synopsis' => $synopsis,
                    'note' => $note,
                    'affiche_film' => $affiche_film,
                    'id_realisateur' => $realisateur
                ]);
    
                $idFilm = $pdo->lastInsertId();
                $requete2 = $pdo->prepare("INSERT INTO appartenir (id_film,id_genre) VALUES (:idFilm,:idGenre)");
                $requete2->execute([
                    'idFilm' => $idFilm,
                    'idGenre' => $libelle
                ]);
            }
            $_SESSION['message'] = "Film has been added successfully";
            
        }
        require_once "ajouterFilm.php";
        
    }
}


?>